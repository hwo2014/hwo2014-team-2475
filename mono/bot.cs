using System;
using System.IO;
using System.Net.Sockets;
using Newtonsoft.Json;

/// <summary>
/// Class to represent a robot that drives a car during a race
/// </summary>
public class Bot {
	public static void Main(string[] args) {
	    string host = args[0];
        int port = int.Parse(args[1]);
        string botName = args[2];
        string botKey = args[3];

	    Bot.inMeasurement = false;
	    Bot.inTesting = false;
        Bot.throtle = 0.65;

	    Bot.Tau = 51.8;

        // *** Add this in the initial tests
	    do
	    {
	        Console.WriteLine("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

	        using (TcpClient client = new TcpClient(host, port))
	        {
	            NetworkStream stream = client.GetStream();
	            StreamReader reader = new StreamReader(stream);
	            StreamWriter writer = new StreamWriter(stream);
	            writer.AutoFlush = true;

	            SendMsg join = null;
	            if (Bot.inTesting)
	            {
	                join = new JoinRace(botName, botKey, Bot.trackName, 1);
	            }
	            else
	            {
	                join = new Join(botName, botKey);
	            }

	            new Bot(reader, writer, join);
	        }

	        Bot.throtle = Bot.throtle + 0.05;
	    } while (Bot.inMeasurement && Bot.throtle <= 1.0);
	}

    private const string trackName = "pentag";

	private StreamWriter writer;

    static bool inTesting;
    static bool inMeasurement;
    static double throtle;

    private static double Tau;

    double previousThrotle;

    string carName;
    string carColor;
    private CarPose myPose;
    private CarPose myPreviousPose;
    private CarPose[] carPoses;

    private Track track;
    private Race race;

    private int totalLaps;
    
    private long ticks;

    private double speed;
    private double prevSpeed;

    private bool accelerationMode;
    private bool crashed;

	Bot(StreamReader reader, StreamWriter writer, SendMsg join) {
		this.writer = writer;

        this.track = new Track();
        this.race = new Race();

        this.initializeRace();

        if (Bot.inTesting)
        {
            this.useTestingStub(reader, join);
        }
        else
        {
            this.processRace(reader, join, null);
        }
	}

    private void useTestingStub(StreamReader reader, SendMsg join)
    {
        var joinRace = join as JoinRace;
        var trackName = Bot.trackName;

        if (joinRace != null)
        {
            trackName = joinRace.trackName;
        }

        using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"C:\temp\HelloWorld\test_output_"+ trackName + "_" + Bot.throtle + ".txt"))
        {
            this.processRace(reader, join, file);
        }
    }

    /// <summary>
    /// Process a race from join to end
    /// </summary>
    /// <param name="reader">The reader</param>
    /// <param name="join">The join command</param>
    /// <param name="file">The file to log traces</param>
    private void processRace(StreamReader reader, SendMsg join, System.IO.StreamWriter file)
    {
        string line;

        this.logMessage(file, true, "{0}, {1}: {2}", this.ticks, 0, "Joining the race");
        send(join);

        while ((line = reader.ReadLine()) != null)
        {
            MsgWrapper msg = JsonConvert.DeserializeObject<MsgWrapper>(line);

            this.processTick(file, msg);
        }
    }

    private void initializeRace()
    {
        this.previousThrotle = 0;
        this.ticks = 0;
        this.speed = 0.0;
        this.prevSpeed = 0.0;

        // Accelerating
        this.accelerationMode = true;
        this.crashed = false;

        this.myPose = null;
        this.myPreviousPose = null;
        this.speed = 0.0;
        this.prevSpeed = 0.0;
    }

    /// <summary>
    /// Process the message received from the server in the current tick
    /// </summary>
    /// <param name="file">File to log the traces</param>
    /// <param name="msg">The message received from the server</param>
    private void processTick(System.IO.StreamWriter file, MsgWrapper msg)
    {
        // this.logMessage(file, false, "{0}, {1}: Message type '{2}' received, with data: '{3}'", this.ticks, 0, msg.msgType, msg.data);
        switch (msg.msgType)
        {
            case "carPositions":
                if (!this.crashed)
                {
                    // This is the "sensors" input
                    // Collecting current positions
                    this.getCarPositions((Newtonsoft.Json.Linq.JArray) msg.data);

                    // Computing control action
                    var action = this.ComputeControlAction(file);

                    send(action);
                }
                else
                {
                    send(new Ping());
                }
                this.ticks++;
                break;
            case "join":
                Console.WriteLine("Joined");
                send(new Ping());
                break;
            case "yourCar":
                var parsedData1 = Newtonsoft.Json.Linq.JObject.Parse(msg.data.ToString());
                this.carName = (string)parsedData1["name"];
                this.carColor = (string)parsedData1["color"];
                send(new Ping());
                break;
            case "gameInit":
                this.logMessage(file, true, "{0}, {1}: Message type '{2}' received, with data: '{3}'", this.ticks, 0, msg.msgType, msg.data);

                var parsedData = Newtonsoft.Json.Linq.JObject.Parse(msg.data.ToString());
                this.race.track = this.track;
                var race = parsedData["race"];
                var track = race["track"];
                this.track.id = (string)track["id"];
                this.track.name = (string)track["name"];
                this.track.pieces = this.getTrackPieces(file, (Newtonsoft.Json.Linq.JArray)track["pieces"]);
                this.track.lanes = this.getTrackLanes((Newtonsoft.Json.Linq.JArray)track["lanes"]);

                // TODO var cars = race["cars"];
                try
                {
                    this.totalLaps = (int) race["raceSession"]["laps"];
                }
                catch (ArgumentNullException)
                {
                    // The message is different in the tests and the actual servers
                    this.totalLaps = 100;
                }

                send(new Ping());
                break;
            case "gameEnd":
                this.logMessage(file, true, "Race ended");
                send(new Ping());
                break;
            case "gameStart":
                this.logMessage(file, true, "Race starts");

                this.initializeRace();

                send(new Ping());
                break;
            case "crash":
                parsedData = Newtonsoft.Json.Linq.JObject.Parse(msg.data.ToString());

                if ((string) parsedData["name"] == this.carName && (string) parsedData["color"] == this.carColor)
                {
                    // TODO: Use this chance to adjust values and reduce chance of a second crash
                    if (file != null)
                    {
                        file.WriteLine();
                    }
                    this.logMessage(file, true, "{0}, {1}: Message type '{2}' received, with data: '{3}'", this.ticks, 0, msg.msgType, msg.data);

                    this.speed = 0.0;
                    this.prevSpeed = 0.0;
                    this.previousThrotle = 0.0;
                    this.accelerationMode = true;
                    this.crashed = true;
                }
                send(new Ping());
                break;
            case "spawn":
                parsedData = Newtonsoft.Json.Linq.JObject.Parse(msg.data.ToString());

                if ((string) parsedData["name"] == this.carName && (string) parsedData["color"] == this.carColor)
                {
                    this.crashed = false;

                    if (file != null)
                    {
                        file.Write(",");
                        file.Write(msg.msgType);
                    }
                }

                send(new Ping());
                break;
            case "lapFinished":
                if (file != null)
                {
                    file.WriteLine();
                }
                this.logMessage(file, true, "{0}, {1}: Message type '{2}' received, with data: '{3}'", this.ticks, 0, msg.msgType, msg.data);
                break;
            default:
                // Good to have this here in case something unexpected is received
                //file.WriteLine("{0}: Message type '{1}' received, with data: '{2}'", System.DateTime.UtcNow.Ticks, msg.msgType, msg.data);
                if (file != null)
                {
                    file.Write(",");
                    file.Write(msg.msgType);
                }
                this.logMessage(file, true, "{0}, {1}: Message type '{2}' received, with data: '{3}'", this.ticks, 0, msg.msgType, msg.data);

                send(new Ping());
                break;
        }
    }

    private void logMessage(System.IO.StreamWriter file, bool duplicate, string format, params object[] args)
    {
        Console.WriteLine(format, args);

        if (file != null && duplicate)
        {
            file.WriteLine(format, args);
        }
    }

    /*
    private double PD_Strategy()
    {
        const double angleThreshold = 55;
        const double speedThreshold = 0.50; // 0.65
        const double kp = (speedThreshold - 0.66) / angleThreshold; //  -0.00291;
        const double kd = -0.065;

        var absAngle = Math.Abs(this.myPose.angle);
        var deltaAngle = Math.Abs(this.myPose.angle) - Math.Abs(this.myPreviousPose.angle);

        if (deltaAngle > 0)
        {
            return kp * absAngle + kd * deltaAngle + 0.65;
        }

        return kp * absAngle + 0.65;
    }

    private double Threshold_Strategy()
    {
        const double angleThreshold0 = 40;
        const double angleThreshold1 = 50;

        var absAngle = Math.Abs(this.myPose.angle);
        var deltaAngle = Math.Abs(this.myPose.angle) - Math.Abs(this.myPreviousPose.angle);

        if (absAngle > angleThreshold1)
        {
            return this.previousThrotle * 0.50;
        } 
        
        if (absAngle > angleThreshold0)
        {
            if (deltaAngle > 0)
            {
                return this.previousThrotle * 0.80;
            }
        }

        if (deltaAngle > 0)
        {
            return this.previousThrotle;
        }

        return Math.Min(this.previousThrotle * 1.01, 1.0);
    }
    */

    private double Threshold_Strategy2()
    {
        var absAngle = Math.Abs(this.myPose.angle);
        var deltaAngle = this.myPreviousPose != null ? Math.Abs(this.myPose.angle) - Math.Abs(this.myPreviousPose.angle) : 0.0;
        double myThrotle;

        if (deltaAngle <= 0.5)
        {
            myThrotle = Math.Max(0.45, Math.Min(this.previousThrotle*1.01, 1.0));
        }
        else
        {
            const double angleThreshold = 55;
            const double speedThreshold = 0.50; // 0.65
            const double kp = (speedThreshold - 0.66)/angleThreshold; //  -0.00291;
            const double kd = -0.065;

            if (deltaAngle > 0)
            {
                myThrotle = kp*absAngle + kd*deltaAngle + 0.45;
            }
            else
            {
                myThrotle = kp*absAngle + 0.45;
            }
        }

        this.accelerationMode = myThrotle >= this.previousThrotle ||
                        this.speed >= this.maxSpeed(this.previousThrotle);

        return myThrotle;
    }

    /// <summary>
    /// Compute a control action based on the current angle of the car. This is applied when the is on a curve.
    /// </summary>
    /// <returns>A throtle level</returns>
    private double ComputeBasedOnAngle(Curve currentPiece, Piece nextPiece, double speed)
    {
        //throtle = this.PD_Strategy(); // TODO: try with this: this.Threshold_Strategy();
        throtle = this.Threshold_Strategy2();

        return throtle;
    }

    private double maxSpeed(double throtle)
    {
        return throtle*10.0;
    }

    private double timeToSafeSpeed(double safeSpeed)
    {
        return -Tau*Math.Log(safeSpeed/this.speed);
    }

    /*
    private double predictDistance(double timeToGo, double maxSpeed)
    {
        return maxSpeed*(timeToGo + Tau*Math.Exp(-timeToGo/Tau));
    }
     * */

    private double distanceToSafeSpeed(double speed0, double timeToSafeSpeed)
    {
        return Tau*speed0* (1 - Math.Exp(-timeToSafeSpeed/Tau));
    }

    private double distanceToNextCurve()
    {
        int indexCurrentPiece = this.myPose.piecePosition.pieceIndex;
        double distanceCurve = ((Straight) this.track.pieces[indexCurrentPiece]).length - this.myPose.piecePosition.inPieceDistance;

        // Last lap, las piece
        if (this.myPose.piecePosition.lap == this.totalLaps - 1)
        {
            int index = indexCurrentPiece + 1;
            bool found = false;
            while (!found && index < this.track.pieces.Length)
            {
                Straight straight = this.track.pieces[index] as Straight;
                if (straight != null)
                {
                    distanceCurve += straight.length;
                    index++;
                }
                else
                {
                    found = true;
                }
            }

            if (!found)
            {
                // No curve ahead
                distanceCurve = 1000000;
            }
        }
        else
        {
            int index = (indexCurrentPiece + 1)%this.track.pieces.Length;
            bool found = false;
            while (!found)
            {
                Straight straight = this.track.pieces[index] as Straight;
                if (straight != null)
                {
                    distanceCurve += straight.length;
                    index = (index + 1)%this.track.pieces.Length;
                }
                else
                {
                    found = true;
                }
            }
        }

        return distanceCurve;
    }

    /// <summary>
    /// Computes the control action based on the current and next piece of the track
    /// </summary>
    /// <returns>A throtle level</returns>
    private double ComputeBasedOnPiece(Straight straight, Piece nextPiece, double speed)
    {
        double throtle;
        double distanceToCurve = this.distanceToNextCurve();

        // Last lap, no curves ahead, run!
        if (distanceToCurve >= 1000000)
        {
            return 1.0;
        }

        // When starting or after a crash
        if (this.speed <= 3.0)
        {
            throtle = ((Bot.throtle == 0) ? 0.4 : Bot.throtle);
            this.accelerationMode = true;
        }
        else
        {
            if (this.accelerationMode)
            {
                // integrate the equation of speed to get the displacement
                // if I stop now, i.e. thrust = 0, how long wil it take me to get to the next curve at a speed of 0.4 at the start of the curve
                // I need the distance to the next curve, my current speed
                double timeTosafeSpeed = this.timeToSafeSpeed(this.maxSpeed(0.45));
                double distanceInTime = this.distanceToSafeSpeed(this.speed, timeTosafeSpeed);

                if (timeTosafeSpeed >= 0 && distanceInTime >= 0.95*distanceToCurve)
                {
                    throtle = 0.0;
                    this.accelerationMode = false;
                }
                else
                {
                    // Compute the maximum speed here that allows us to accelerate and stop before getting to the next curve
                    throtle = Math.Max(0.45, this.previousThrotle * 1.01);
                    this.accelerationMode = true;
                }
            }
            else
            {
                throtle = 0.0;
            }
        }

        return throtle;
    }

    /*
    /// <summary>
    /// Computes the control action based on the current and next piece of the track
    /// </summary>
    /// <param name="file">A file to store traces if needed</param>
    /// <returns>A throtle level</returns>
    private double ComputeBasedOnPieceOld(Straight straight, Piece nextPiece, double speed)
    {
        var distanceInPiece = this.myPose.piecePosition.inPieceDistance;

        double throtle = (this.previousThrotle <= 0.0) ? ((Bot.throtle == 0) ? 0.5 : Bot.throtle) : this.previousThrotle; // 0.75; //  0.78; // 0.75

        var next = nextPiece as Straight;
        if (next == null)
        {
            // double k = 0.9 * 100.0;
            // double reductByRadius = 1.0 / nextCurve.radius;
            if (distanceInPiece / straight.length >= 0.75) // 0.75
            {
                throtle = 0.64; //throtle * k * reductByRadius; 0.65
            }

            // throtle = Math.Max((0.72 - 0.75) / (0.8 * straight.length) * distanceInPiece + 0.75, 0.72);
        }

        return throtle;
    }*/

    private void computeSpeed()
    {
        this.prevSpeed = this.speed;

        double distance = this.myPose.piecePosition.inPieceDistance;
        if (this.myPreviousPose != null)
        {
            if (this.myPreviousPose.piecePosition.pieceIndex == this.myPose.piecePosition.pieceIndex)
            {
                distance -= this.myPreviousPose.piecePosition.inPieceDistance;
            }
            else
            {
                Straight straightPrev = this.track.pieces[this.myPreviousPose.piecePosition.pieceIndex] as Straight;
                if (straightPrev != null)
                {
                    distance += (straightPrev.length - this.myPreviousPose.piecePosition.inPieceDistance);
                }
                else
                {
                    // If previous is curve, the formula below does not seem to reflect the data obtained from the messages. So use the previous speed here.
                    distance = this.prevSpeed;

                    /*
                    Curve curve = (Curve) this.track.pieces[this.myPreviousPose.piecePosition.pieceIndex];
                    distance = ((curve.angle*180.0/Math.PI*curve.radius -
                                 this.myPreviousPose.piecePosition.inPieceDistance) +
                                this.myPose.piecePosition.inPieceDistance);
                     */
                }
            }
        }

        // This is computed every tick, so the denominator is 1. That's why the speed in linearUnits/timeUnits equals the distance in linearUnits
        this.speed = distance;
    }

    /// <summary>
    /// Computes the control action for the current tick
    /// </summary>
    /// <param name="file">A file to log data (if needed)</param>
    /// <returns>The message to send to the server. The message contains the control action.</returns>
    private SendMsg ComputeControlAction(System.IO.StreamWriter file)
    {
        double throtle;
        var currentPieceIdx = this.myPose.piecePosition.pieceIndex;
        var straight = this.track.pieces[currentPieceIdx] as Straight;
        var currentIsStraight = straight != null;

        var nextPieceIdx = (currentPieceIdx + 1) % this.track.pieces.Length;
        var straightNext = this.track.pieces[nextPieceIdx] as Straight;
        var nextIsStraight = straightNext != null;

        this.computeSpeed();

        if (!Bot.inMeasurement)
        {
            if (currentIsStraight)
            {
                throtle = this.ComputeBasedOnPiece(straight, this.track.pieces[nextPieceIdx], speed);
            }
            else
            {
                throtle = this.ComputeBasedOnAngle(this.track.pieces[currentPieceIdx] as Curve,
                                                   this.track.pieces[nextPieceIdx], speed);
            }
        }
        else
        {
            throtle = Bot.throtle;
        }

        /*
            if (!straightNext)
            {
                // Next is a curve
                Curve nextCurve = this.track.pieces[nextPieceIdx] as Curve;

                // Assuming there is only one switch per piece, adn that the information is updted as soon as the fact takes place
                if (this.myPose.piecePosition.lane.startLaneIndex == this.myPose.piecePosition.lane.endLaneIndex && straight.switchAvailable)
                {
                    if (nextCurve.angle < 0)
                    {
                        // Curve left
                        if (this.track.lanes[this.myPose.piecePosition.lane.startLaneIndex] < 0)
                        {
                            this.myPreviousPose = this.myPose;

                            // extend to the right
                            return new SwitchLane("Right");
                        }
                    }
                    // Curve right 
                    else if (this.track.lanes[this.myPose.piecePosition.lane.startLaneIndex] > 0)
                    {
                        this.myPreviousPose = this.myPose;

                        // Extende to the left
                        return new SwitchLane("Left");                        
                    }
                }
            }

            throtle = this.ComputeBasedOnPiece(straight, this.track.pieces[nextPieceIdx], speed);
         */

        this.printData(file, throtle, currentIsStraight ? "Straight" : "Curve", nextIsStraight ? "Straight" : "Curve", speed);

        this.myPreviousPose = this.myPose;
        this.previousThrotle = throtle;

        return new Throttle(Math.Min(1.0, Math.Max(0.0, throtle)));
    }

    /// <summary>
    /// Prints a line of data if the file is not null
    /// </summary>
    /// <param name="file">The file</param>
    /// <param name="throtle">The value of throtle</param>
    /// <param name="current">Type (string) of the current piece of the track ("Straight" or "Curve")</param>
    /// <param name="next">Type (string) of the next piece of the track ("Straight" or "Curve")</param>
    private void printData(System.IO.StreamWriter file, double throtle, string current, string next, double speed)
    {
        if (this.ticks == 0)
        {
            Console.WriteLine("Current,Ticks,Millis,angle,piece,inPieceDistance,throtle,prevTrothle,lap,next,displacement/speed");
            if (file != null)
            {
                file.WriteLine("Current,Ticks,Millis,angle,piece,inPieceDistance,throtle,prevTrothle,lap,next,displacement/speed");
            }
        }

        Console.WriteLine(
                    "{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10}",
                    current,
                    this.ticks,
                    0,
                    this.myPose.angle,
                    this.myPose.piecePosition.pieceIndex,
                    this.myPose.piecePosition.inPieceDistance,
                    throtle,
                    this.previousThrotle,
                    this.myPose.piecePosition.lap,
                    next,
                    speed);

        if (file != null)
        {
            file.WriteLine();
            file.Write(
                string.Format(
                    "{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10}",
                    current,
                    this.ticks,
                    0,
                    this.myPose.angle,
                    this.myPose.piecePosition.pieceIndex,
                    this.myPose.piecePosition.inPieceDistance,
                    throtle,
                    this.previousThrotle,
                    this.myPose.piecePosition.lap,
                    next,
                    speed));
        }

    }

    /// <summary>
    /// Reads and converts the "car positions" message from the server and stors the results in memory
    /// </summary>
    /// <param name="carPoses">The array of car poses from the server</param>
    private void getCarPositions(Newtonsoft.Json.Linq.JArray carPoses)
    {
        this.carPoses = new CarPose[carPoses.Count];
        int i = 0;
        foreach (var car in carPoses)
        {
            this.carPoses[i] = new CarPose
            {
                id = new CarId
                {
                    name = (string)car["id"]["name"],
                    color = (string)car["id"]["color"],
                },
                angle = (double)car["angle"],
                piecePosition = new PiecePosition
                {
                    pieceIndex = (int)car["piecePosition"]["pieceIndex"],
                    inPieceDistance = (double)car["piecePosition"]["inPieceDistance"],
                    lane = new LaneStatus
                    {
                        startLaneIndex = (int)car["piecePosition"]["lane"]["startLaneIndex"],
                        endLaneIndex = (int)car["piecePosition"]["lane"]["endLaneIndex"],
                    },
                    lap = (int)car["piecePosition"]["lap"],
                },
            };

            if (this.carPoses[i].id.name == this.carName && this.carPoses[i].id.color == this.carColor)
            {
                this.myPose = this.carPoses[i];
            }
        }
    }

    /// <summary>
    /// Reads and converts the track pieces from the gameInit message
    /// </summary>
    /// <param name="trackPieces">The array with the track pieces</param>
    /// <returns>An array of Piece objects</returns>
    private Piece[] getTrackPieces(System.IO.StreamWriter file, Newtonsoft.Json.Linq.JArray trackPieces)
    {
        var trackP = new Piece[trackPieces.Count];
        int i = 0;
        foreach (var piece in trackPieces)
        {
            if (file != null)
            {
                file.Write(i);
                file.Write(",");
            }

            if (piece["length"] != null)
            {
                trackP[i] = new Straight
                {
                    length = double.Parse((string)piece["length"]),
                };

                if (file != null)
                {
                    file.Write("Straight,");
                    file.Write(piece["length"]);
                    file.Write(",0,");
                }
            }
            else
            {
                trackP[i] = new Curve
                {
                    radius = double.Parse((string)piece["radius"]),
                    angle = double.Parse((string)piece["angle"]),
                };

                if (file != null)
                {
                    file.Write("Curve,");
                    file.Write(piece["radius"]);
                    file.Write(",");
                    file.Write(piece["angle"]);
                    file.Write(",");
                }
            }

            trackP[i].switchAvailable = piece["switch"] != null;

            if (file != null)
            {
                file.WriteLine(trackP[i++].switchAvailable ? "switch" : "");
            }
        }

        return trackP;
    }

    /// <summary>
    /// Reads and converts the track lanes from the gameInit message
    /// </summary>
    /// <param name="trackPieces">The array with the track pieces</param>
    /// <returns>An array of Piece objects</returns>
    private double[] getTrackLanes(Newtonsoft.Json.Linq.JArray trackLanes)
    {
        var trackL = new double[trackLanes.Count];
        foreach (var lane in trackLanes)
        {
            trackL[(int)lane["index"]] = (double)lane["distanceFromCenter"];
        }

        return trackL;
    }

    /// <summary>
    /// Sends a serialized message to the server. The server is represented by a StreamWriter
    /// </summary>
    /// <param name="msg">The message to send to the server</param>
	private void send(SendMsg msg) {
		writer.WriteLine(msg.ToJson());
	}
}

class MsgWrapper {
    public string msgType;
    public Object data;
    // public string gameId;

    public MsgWrapper(string msgType, Object data) {
    	this.msgType = msgType;
    	this.data = data;
    }
}

abstract class SendMsg {
	public string ToJson() {
		return JsonConvert.SerializeObject(new MsgWrapper(this.MsgType(), this.MsgData()));
	}
	protected virtual Object MsgData() {
        return this;
    }

    protected abstract string MsgType();
}

class Join: SendMsg {
	public string name;
	public string key;
	public string color;

	public Join(string name, string key) {
		this.name = name;
		this.key = key;
		this.color = "red";
	}

	protected override string MsgType() { 
		return "join";
	}
}

class BotId
{
    public string name;
    public string key;
}

class JoinRace : SendMsg
{
    public BotId botId;
    public string trackName;
    public int carCount;

    public JoinRace(string botName, string botKey, string trackName, int carCount)
    {
        this.botId = new BotId()
            {
                name = botName,
                key = botKey,
            };
        this.trackName = trackName;
        this.carCount = carCount;
    }

    protected override string MsgType()
    {
        return "joinRace";
    }
}

class Ping: SendMsg {
	protected override string MsgType() {
		return "ping";
	}
}

/// <summary>
/// A message to request for a throtle level from the server
/// </summary>
class Throttle: SendMsg {
	public double value;

	public Throttle(double value) {
		this.value = value;
	}

	protected override Object MsgData() {
		return this.value;
	}

	protected override string MsgType() {
		return "throttle";
	}
}

class SwitchLane : SendMsg
{
    public string side;

    public SwitchLane(string value) {
		this.side = value;
	}

	protected override Object MsgData() {
		return this.side;
	}

	protected override string MsgType() {
		return "switchLane";
	}
}

/// <summary>
/// A car Id including name and color
/// </summary>
class CarId {
    public string name;
    public string color;
}

/// <summary>
/// The lane status
/// </summary>
class LaneStatus {
    public int startLaneIndex;
    public int endLaneIndex;
}

/// <summary>
/// The piece position object
/// </summary>
class PiecePosition
{
    public int pieceIndex;
    public double inPieceDistance;
    public LaneStatus lane;
    public int lap;
}

/// <summary>
/// The car pose (a.k.a. car position)
/// </summary>
class CarPose {
    public CarId id;
    public double angle;
    public PiecePosition piecePosition;
}

/// <summary>
/// Abstract class for a piece of the track
/// </summary>
abstract class Piece
{
    public bool switchAvailable;
}

/// <summary>
/// A straight section of the track
/// </summary>
class Straight : Piece
{
    public double length;
}

/// <summary>
/// A curved section of the track
/// </summary>
class Curve : Piece
{
    public double radius;
    public double angle;
}

/* // Inconsistent spec for the car position
class Position 
{
    double x;
    double y;
}

/// <summary>
/// The starting point
/// </summary>
class StartingPoint
{
    Position position;
    double angle;
}

/// <summary>
/// The car dimensions
/// </summary>
class CarDimensions
{
    double length;
    double width;
    double guideFlagPosition;
}

/// <summary>
/// The car Id and its dimensions
/// </summary>
class Car
{
    CarId carId;
    CarDimensions carDimensions;
}
*/
 
/// <summary>
/// Stores a description fo the track as it is discovered from the messages
/// </summary>
class Track
{
    public string id;
    public string name;
    public Piece[] pieces;
    public double[] lanes;

    // StartingPoint startingPoint;
}

/*
/// <summary>
/// The Race Session object
/// </summary>
class RaceSesion
{
    int laps;
    int maxLapTimeMs;
    bool quickRace;
}
*/

/// <summary>
/// The Race object including the track description, the list of cars involved and the race session
/// </summary>
class Race
{
    public Track track;
    //public Car[] cars;
    //public RaceSesion raceSession;
}